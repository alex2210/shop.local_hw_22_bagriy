<?php

use yii\db\Migration;

/**
 * Class m190318_031420_add_order
 */
class m190318_031420_add_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->insert('orders', [
			'customer_name' => 'Вася Пупкин',
			'email' => 'pupkin@gmail.com',
			'phone' => '223-134',
			'feedback' => 'Отличный товар'
		]);
		$this->insert('orders', [
			'customer_name' => 'Иван Иванов',
			'email' => 'ivanov@gmail.com',
			'phone' => '532-259',
			'feedback' => 'Хороший товар'
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190318_031420_add_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190318_031420_add_order cannot be reverted.\n";

        return false;
    }
    */
}
