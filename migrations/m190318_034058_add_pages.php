<?php

use yii\db\Migration;

/**
 * Class m190318_034058_add_pages
 */
class m190318_034058_add_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->insert('pages', [
			'title' => 'Товары',
			'alias' => 'Products',
			'intro' => 'Страница с товарами',
			'content' => 'Test 2'
		]);
		$this->insert('pages', [
			'title' => 'Заказы',
			'alias' => 'Orders',
			'intro' => 'Страница заказов',
			'content' => 'Test 2'
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190318_034058_add_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190318_034058_add_pages cannot be reverted.\n";

        return false;
    }
    */
}
