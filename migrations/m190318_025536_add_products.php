<?php

use yii\db\Migration;

/**
 * Class m190318_025536_add_products
 */
class m190318_025536_add_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->insert('products', [
			'title' => 'MacBook',
			'alias' => 'MacBook',
			'price' => '25000',
			'description' => 'Intel Core i5 (1.8 - 2.9 ГГц) / RAM 8 ГБ / SSD 128 ГБ / Intel HD Graphics 6000 / без ОД / Wi-Fi'

		]);
		$this->insert('products', [
			'title' => 'iPhone X',
			'alias' => 'iPhone X',
			'price' => '28000',
			'description' => 'Apple iPhone X 64gb Space Gray',

		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190318_025536_add_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190318_025536_add_products cannot be reverted.\n";

        return false;
    }
    */
}
