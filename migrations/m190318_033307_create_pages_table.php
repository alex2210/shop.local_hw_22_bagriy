<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pages}}`.
 */
class m190318_033307_create_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('pages', [
			'id' => $this->primaryKey(),
			'title' => $this->string(),
			'alias' => $this->string(),
			'intro' => $this->text(),
			'content' => $this->text(),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }
}
