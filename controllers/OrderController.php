<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 18.03.2019
	 * Time: 6:58
	 */

	namespace app\controllers;


	use app\models\Orders;
	use yii\web\Controller;

	class OrderController extends Controller
	{
		public function actionIndex()
		{
			$orders = Orders::find()->all();

			return $this->render('index',[
				'title' => 'Заказы',
				'orders' => $orders,
			]);
		}
	}