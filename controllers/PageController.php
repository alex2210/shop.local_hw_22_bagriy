<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 18.03.2019
	 * Time: 6:58
	 */

	namespace app\controllers;


	use app\models\Orders;
	use app\models\Pages;
	use yii\web\Controller;

	class PageController extends Controller
	{
		public function actionIndex()
		{
			$pages = Pages::find()->all();

			return $this->render('index', [
				'title' => 'Страницы',
				'pages' => $pages,
			]);
		}

		public function actionView($id)
		{
			$pages = Pages::findOne($id);

			return $this->render('view', [
				'pages' => $pages,
			]);
		}
	}