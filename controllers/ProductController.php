<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 18.03.2019
	 * Time: 6:57
	 */

	namespace app\controllers;

	use app\models\Products;
	use yii\web\Controller;

	class ProductController extends Controller
	{
		public function actionIndex()
		{
			$products = Products::find()->all();

			return $this->render('index', [
				'title' => 'Товары',
				'products' => $products,
			]);
		}
		public function actionView($id)
		{
			$products = Products::findOne($id);

			return $this->render('view', [
				'products' => $products,
			]);
		}
	}