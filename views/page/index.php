<?php
	/**
	 * @var string $title
	 * @var \app\models\Pages $pages
	 */
	$this->title = 'Страницы';
?>

	<h1><?= $title ?></h1>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Страница</th>
            <th>Вступление</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($pages as $page) :?>
            <tr>
                <td><?= $page->id ?></td>
                <td>
                    <?= \yii\helpers\Html::a($page->title, ['product/index'],
                         ['class' => 'badge badge-info']) ?>
                </td>
                <td><?= $page->intro ?></td>
                <td><?= \yii\helpers\Html::a(' Посмотреть информацию',
                        ['page/view', 'id' => $page->id ],
                        ['class' => 'badge badge-info']) ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>

</table>




