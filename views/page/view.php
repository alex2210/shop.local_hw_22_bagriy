<?php
	/**
	 * @var \app\models\Pages $pages
	 */
	$this->title = $pages->alias;
?>

<p class="h2"><?= $pages->title ?></p>

<p>
	<?= $pages->content ?>
</p>