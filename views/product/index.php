<?php
	/**
	 * @var string $title
	 * @var \app\models\Products $products
	 */
	$this->title = 'Товары';
?>

<h1><?= $title ?></h1>


	<?php foreach ($products as $product) : ?>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">
				<?= '<b>Названия товара:</b> ' . $product->title . ' <b>Цена:</b> '
					. $product->price . \yii\helpers\Html::a(' Посмотреть информацию',[
					        'product/view', 'id' => $product->id ],
					         ['class' => 'badge badge-info']) . '<br>'?>
			</li>
		</ul>
	<?php endforeach ?>
